const API = require('./method')

module.exports = {
  name: 'artefact',
  version: require('./package.json').version,
  manifest: {
    create: 'async',
    get: 'async',
    update: 'async'
  },
  init: (server) => {
    if (!server.backlinks) throw new Error('ssb-artefact requires ssb-backlinks')
    return API(server)
  }
}
