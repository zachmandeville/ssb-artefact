const { DateChecker } = require('./lib/date-checkers')
const { string, integer } = require('./lib/field-types')
const { blobSchema, hyperBlobSchema } = require('./lib/field-schemas')

function Spec (type) {
  const spec = {
    type,
    tangle: 'artefact',
    staticProps: {
      blob: {
        required: true,
        oneOf: [blobSchema, hyperBlobSchema]
      }
    },
    props: {
      title: string,
      description: string,
      createdAt: string,
      identifier: string,
      licence: string,
      rights: string,
      source: string,
      language: string,
      translation: string,
      location: string
    },
    hooks: {
      isRoot: [
        DateChecker('createdAt')
      ],
      isUpdate: [
        DateChecker('createdAt')
      ]
    }
  }

  // inject the additional properties specific to video and audio artefacts into the spec
  if (type === 'artefact/video' || type === 'artefact/audio') {
    spec.props = {
      ...spec.props,
      duration: integer,
      transcription: string
    }
  }

  return spec
}

module.exports = Spec
