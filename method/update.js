module.exports = function Update (server, loadCrut) {
  return function update (id, input, cb) {
    loadCrut(id, (err, crut) => {
      if (err) return cb(err)

      crut.update(id, input, cb)
    })
  }
}
